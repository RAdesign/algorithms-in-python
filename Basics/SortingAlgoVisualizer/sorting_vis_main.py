"""bubble sort vs insertion sort visualizer - to do : add timer selection option"""
import pygame
import random
import math

pygame.init()


class DrawInfo:
    black = 0, 0, 0
    white = 255, 255, 255
    green = 0, 255, 0
    red = 255, 0, 0
    bckgrnd_colour = white

    gradients = [
        (120, 120, 120),
        (150, 150, 150),
        (180, 180, 180)
    ]

    font = pygame.font.SysFont('tahoma', 10)
    big_font = pygame.font.SysFont('tahoma', 20)

    side_padding = 100
    top_padding = 150

    def __init__(self, width, height, the_lst):
        self.width = width
        self.height = height

        self.window = pygame.display.set_mode((width, height))
        pygame.display.set_caption("Sorting Algorithms Visualizer")
        self.set_list(the_lst)

    def set_list(self, the_lst):
        self.the_lst = the_lst
        self.min_val = min(the_lst)
        self.max_val = max(the_lst)

        self.block_width = round((self.width - self.side_padding) / len(the_lst))
        self.block_height = math.floor((self.height - self.top_padding) / (self.max_val - self.min_val))
        self.start_x = self.side_padding // 2


def draw(draw_info, algo_name, ascending):
    draw_info.window.fill(draw_info.bckgrnd_colour)

    title = draw_info.big_font.render(f"{algo_name} - {'Ascending' if ascending else 'Descending'}", 1, draw_info.black)
    draw_info.window.blit(title, (draw_info.width / 2 - title.get_width() / 2, 5))

    controls = draw_info.font.render("R - reset | A - by ascending | D - by descending", 1, draw_info.black)
    draw_info.window.blit(controls, (draw_info.width / 2 - controls.get_width() / 2, 45))

    sorting = draw_info.font.render("B - bubble sort | I - insertion sort | Space - sort", 1, draw_info.black)
    draw_info.window.blit(sorting, (draw_info.width / 2 - sorting.get_width() / 2, 75))

    draw_list(draw_info)
    pygame.display.update()


def draw_list(draw_info, color_positions={}, clear_bg=False):
    the_lst = draw_info.the_lst

    if clear_bg:
        clear_rect = (draw_info.side_padding // 2, draw_info.top_padding, draw_info.width - draw_info.side_padding,
                      draw_info.height - draw_info.top_padding)
        pygame.draw.rect(draw_info.window, draw_info.bckgrnd_colour, clear_rect)

    for i, val in enumerate(the_lst):
        x = draw_info.start_x + i * draw_info.block_width
        y = draw_info.height - (val - draw_info.min_val) * draw_info.block_height

        color = draw_info.gradients[i % 3]

        if i in color_positions:
            color = color_positions[i]

        pygame.draw.rect(draw_info.window, color, (x, y, draw_info.block_width, draw_info.height))

    if clear_bg:
        pygame.display.update()


def generate_starting_list(n, min_val, max_val):
    the_lst = []

    for _ in range(n):
        val = random.randint(min_val, max_val)
        the_lst.append(val)

    return the_lst


def bubble_sort(draw_info, ascending=True):
    the_lst = draw_info.the_lst

    for i in range(len(the_lst) - 1):
        for j in range(len(the_lst) - 1 - i):
            num1 = the_lst[j]
            num2 = the_lst[j + 1]

            if (num1 > num2 and ascending) or (num1 < num2 and not ascending):
                the_lst[j], the_lst[j + 1] = the_lst[j + 1], the_lst[j]
                draw_list(draw_info, {j: draw_info.green, j + 1: draw_info.red}, True)
                yield True

    return the_lst


def insertion_sort(draw_info, ascending=True):
    the_lst = draw_info.the_lst

    for i in range(1, len(the_lst)):
        current = the_lst[i]

        while True:
            ascending_sort = i > 0 and the_lst[i - 1] > current and ascending
            descending_sort = i > 0 and the_lst[i - 1] < current and not ascending

            if not ascending_sort and not descending_sort:
                break

            the_lst[i] = the_lst[i - 1]
            i = i - 1
            the_lst[i] = current
            draw_list(draw_info, {i - 1: draw_info.green, i: draw_info.red}, True)
            yield True

    return the_lst


def main():
    run = True
    clock = pygame.time.Clock()

    n = 20
    min_val = 0
    max_val = 100

    the_lst = generate_starting_list(n, min_val, max_val)
    draw_info = DrawInfo(900, 700, the_lst)
    sorting = False
    ascending = True

    sorting_algorithm = bubble_sort
    sorting_algo_name = "Bubble Sort"
    sorting_algorithm_generator = None

    while run:
        clock.tick(2)

        if sorting:
            try:
                next(sorting_algorithm_generator)
            except StopIteration:
                sorting = False
        else:
            draw(draw_info, sorting_algo_name, ascending)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False

            if event.type != pygame.KEYDOWN:
                continue

            if event.key == pygame.K_r:
                the_lst = generate_starting_list(n, min_val, max_val)
                draw_info.set_list(the_lst)
                sorting = False
            elif event.key == pygame.K_SPACE and sorting == False:
                sorting = True
                sorting_algorithm_generator = sorting_algorithm(draw_info, ascending)
            elif event.key == pygame.K_a and not sorting:
                ascending = True
            elif event.key == pygame.K_d and not sorting:
                ascending = False
            elif event.key == pygame.K_i and not sorting:
                sorting_algorithm = insertion_sort
                sorting_algo_name = "Insertion Sort"
            elif event.key == pygame.K_b and not sorting:
                sorting_algorithm = bubble_sort
                sorting_algo_name = "Bubble Sort"

    pygame.quit()


if __name__ == "__main__":
    main()
