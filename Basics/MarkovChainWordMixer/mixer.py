import os
import re
import string
import random
from markov_graph import Graph, Vertex


def get_words_from_text(text_path):
    with open(text_path, 'rb') as file:
        text = file.read().decode("utf-8")

        # text = re.sub(r'\[(.+)\]', ' ', text)  # regex - replace titles with spaces

        text = ' '.join(text.split())  # remove all large whitespaces (next lines, tabs) into 1 char spaces
        text = text.lower()
        text = text.translate(str.maketrans('', '', string.punctuation))  # replace any punctuation with empty strings

    words = text.split()

    words = words[:5000]  # to manage memory not to overdo with words

    return words


def make_graph(words):
    grt = Graph()
    prev_word = None
    for word in words:
        # check that each word is in graph, and if not then add it
        word_vertex = grt.get_vertex(word)

        # if there was a previous word, then add an edge if it had no edges before
        # if exists, increment weight(edge) by 1
        if prev_word:  # prev word should be a Vertex
            # check if edge exists from previous word to current word
            prev_word.increment_edge(word_vertex)

        prev_word = word_vertex

    grt.generate_probability_mapping()

    return grt


def compose(grt, words, length=50):
    composition = []  # empty list first
    word = grt.get_vertex(random.choice(words))  # get a random starting word from words
    for _ in range(length):  # length parameter for how many iterations of getting a new word
        composition.append(word.value)  # append current word
        word = grt.get_next_word(word)  # word var is a new word chosen from graph based on weighted P

    return composition


def main():
    # words = get_words_from_text('songs/all_songs.txt')

    # get all words from files in a directory
    words = []
    for song_file in os.listdir('songs/lana_del_rey/'):
        song_words = get_words_from_text(f'songs/lana_del_rey/{song_file}')
        words.extend(song_words)

    grt = make_graph(words)
    composition = compose(grt, words, 60)
    print(' '.join(composition))  # change list to a string of words


if __name__ == '__main__':
    main()