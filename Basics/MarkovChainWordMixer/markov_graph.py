"""Improvements : get more than 1 next word from data"""
import random


class Vertex(object):  # a node class
    def __init__(self, value):
        self.value = value
        self.adjacent = {}  # nodes pointed at by this one are the keys
        self.neighbors = []  # to track neighboring vertices of this vertex
        self.neighbors_weights = []  # to track neighboring vertices weights

    def __str__(self):  # for nice drawing
        return self.value + ' '.join([node.value for node in self.adjacent.keys()])

    def add_edge_to_vertex(self, vertex, weight=0):  # adding new words connections
        self.adjacent[vertex] = weight

    def increment_edge(self, vertex):  # for repeating words - add weight
        self.adjacent[vertex] = self.adjacent.get(vertex, 0) + 1

    def get_adjacent_nodes(self):
        return self.adjacent.keys()

    def get_map_of_probability(self):  # create parameters for weighted random.choices
        for (vertex, weight) in self.adjacent.items():
            self.neighbors.append(vertex)  # to use as param for random.choices
            self.neighbors_weights.append(weight)  # second param for random.choices

    def go_to_next_word(self):  # select another vertex, by combining randomness and weight of edges
        # might be improved to get 2, or with some more params
        return random.choices(self.neighbors, weights=self.neighbors_weights)[0]


class Graph(object):
    def __init__(self):
        self.vertices = {}  # dictionary of words, after adding - will serve for adding weights

    def get_vertex_value(self):  # what are values of all vertices - all word-nodes already added
        return set(self.vertices.keys())

    def add_vertex(self, value):  # for adding new word-vertices
        self.vertices[value] = Vertex(value)

    def get_vertex(self, value):  # for a word, to get a vertex object, that it represents
        if value not in self.vertices:  # if the value is not in the graph, add it (just in case condition)
            self.add_vertex(value)
        return self.vertices[value]  # get the vertex object

    def get_next_word(self, current_vertex):  # get next word(vertex), based on current vertex and edges weights
        return self.vertices[current_vertex.value].go_to_next_word()

    def generate_probability_mapping(self):  # generate probability maps for all vertices in a graph
        for vertex in self.vertices.values():
            vertex.get_map_of_probability()
